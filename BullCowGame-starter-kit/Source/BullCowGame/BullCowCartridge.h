// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "BullCowCartridge.generated.h"

struct FBullCowCount {
	int32 Bulls = 0;
	int32 Cows = 0;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;

	// Your declarations go below!
private:
	// The welcoming messages to be displayed
	const TArray<FString> WelcomeMessages = {
		TEXT("Welcome to the shittiest game ever!"),
		TEXT("Press TAB, enter your message and ENTER.")
	};

	// All of the possible hidden words
	const TArray<FString> HiddenWordsArray = {
		TEXT("cows"), TEXT("rift"), TEXT("deathwing")
	};

	const FString WinMessage = TEXT("You won!");
	const FString LoseMessage = TEXT("You lost!");
	const FString PlayAgainMessage = TEXT("If you want to play again press ENTER.\nIf not, press ESC to quit.");

	// The actual hidden word
	FString HiddenWord = TEXT("");
	// The number of lives (size of the word)
	int32 CurrentLives = 0;

	bool bGameOver = false;

	// Pick a new hidden word from the list randomly
	void PickNewHiddenWord();

	// Print the welcoming messages, the word information etc
	void PrintWelcomingMessages();
	void PrintWordMessages();

	// Checks if the user input equals the hidden word and shows if true or not
	void CheckUserInput(const FString& input);

	// Check the Input
	bool CheckInputIsNotIsogram(const FString& input) const;
	bool CheckInputIsWrongLength(const FString& input) const;

	// Checks the bulls and cows
	FBullCowCount GetBullCows(const FString& Guess) const;
};
