// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
	PrintWelcomingMessages();
	PickNewHiddenWord();
	bGameOver = false;
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	ClearScreen();
	CheckUserInput(Input);
}

void UBullCowCartridge::PickNewHiddenWord()
{
	// Pick new hidden word and set current lives to its size
	HiddenWord = HiddenWordsArray[rand() % HiddenWordsArray.Max()];
	CurrentLives = HiddenWord.Len();

	//Prints the word information each time it is a new one.
	PrintWordMessages();
}

void UBullCowCartridge::PrintWelcomingMessages()
{
	for (auto msg : WelcomeMessages)
		PrintLine(msg);
}

void UBullCowCartridge::PrintWordMessages() {
	PrintLine("You are guessing a " + FString::FromInt(HiddenWord.Len()) + " letter word.");
	//PrintLine("You currently have " + FString::FromInt(CurrentLives) + " lives.");
	PrintLine(TEXT("You currently have %i lives."), CurrentLives);
}

void UBullCowCartridge::CheckUserInput(const FString& Input)
{
	if (bGameOver && Input == "") {
		PrintLine(TEXT("Thanks for playing again!"));
		PickNewHiddenWord();
		bGameOver = false;
		return;
	}

	bool ChecksAreWrong = false;

	// Check if isogram
	ChecksAreWrong |= CheckInputIsNotIsogram(Input);
	// Check right number of characters
	ChecksAreWrong |= CheckInputIsWrongLength(Input);

	if (ChecksAreWrong) {
		PrintLine("Try again.");
		PrintWordMessages();
		return;
	}

	// Win & Lose conditions
	if (Input == HiddenWord) {
		PrintLine(WinMessage);
		PrintLine(PlayAgainMessage);
		bGameOver = true;
		return;
	}
	else {
		PrintLine(TEXT("Wrong!"));
		if (--CurrentLives == 0) {
			PrintLine(LoseMessage);
			PrintLine(TEXT("The hidden word was: %s"), *HiddenWord);
			PrintLine(PlayAgainMessage);
			bGameOver = true;
			return;
		}

		FBullCowCount BCCount = GetBullCows(Input);

		PrintLine(TEXT("You have %i bulls and %i cows"), BCCount.Bulls, BCCount.Cows);
	}

	PrintWordMessages();
}

bool UBullCowCartridge::CheckInputIsNotIsogram(const FString& Input) const {
	// Iterate each letter of the word
	for (int i = 0; i < Input.Len(); i++)
	{
		// Iterate the letter against every other one
		for (int j = i + 1; j < Input.Len(); j++) {
			// If two letters are the same, the word is an isogram
			if (Input[i] == Input[j]) {
				PrintLine(TEXT("Your word is not an isogram, the letter %c is repeated"), Input[i]);
				return true;
			}
		}
	}
	return false;
}

bool UBullCowCartridge::CheckInputIsWrongLength(const FString& Input) const {
	if (Input.Len() != HiddenWord.Len()) {
		PrintLine("Your word doesn't have exactly " + FString::FromInt(HiddenWord.Len()) + " chars!");
		return true;
	}
	return false;
}

FBullCowCount UBullCowCartridge::GetBullCows(const FString& Guess) const {
	FBullCowCount BCCount;
	// For every char from input if same as hidden bull++
	// if not a bull is a cow?

	for (int32 i = 0; i < Guess.Len(); i++) {
		if (Guess[i] == HiddenWord[i]) {
			BCCount.Bulls++;
			continue;
		}

		for (int32 j = 0; j < HiddenWord.Len(); j++) {
			if (Guess[i] == HiddenWord[j]) {
				BCCount.Cows++;
				break;
			}
		}
	}

	return BCCount;
}
