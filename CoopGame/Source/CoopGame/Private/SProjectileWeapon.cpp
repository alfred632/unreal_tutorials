// Fill out your copyright notice in the Description page of Project Settings.


#include "SProjectileWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "SProjectile.h"
#include "Net/UnrealNetwork.h"

void ASProjectileWeapon::PlayFireEffect()
{
	if (MuzzleEffect)
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
}

void ASProjectileWeapon::OnRep_HitScanTrace()
{
	PlayFireEffect();
}

void ASProjectileWeapon::Fire()
{
	AActor* MyOwner = GetOwner();
	if (!MyOwner)
		return;

	if (HasAuthority())
	{
		if (LastFireTime + TimeBetweenShots > GetWorld()->GetTimeSeconds())
			return;

		LastFireTime = GetWorld()->GetTimeSeconds();
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FActorSpawnParameters ActorSpawnParameters;
		ActorSpawnParameters.Owner = this;
		ActorSpawnParameters.Instigator = MyOwner->GetInstigatorController()->GetPawn();
		ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		AActor* Projectile = GetWorld()->SpawnActor<AActor>(ProjectileClass, MuzzleLocation, EyeRotation, ActorSpawnParameters);

		HitScanTrace.Counter = ++HitScanTrace.Counter == 255 ? 0 : HitScanTrace.Counter++;

		PlayFireEffect();
	}	
}

void ASProjectileWeapon::StartFire()
{
	Fire();
}

void ASProjectileWeapon::StopFire()
{

}
