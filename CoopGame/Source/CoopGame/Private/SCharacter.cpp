// Fill out your copyright notice in the Description page of Project Settings.


#include "SCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "CoopGame/CoopGame.h"
#include "CoopGame/Components/SHealthComponent.h"
#include "SWeapon.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ASCharacter::ASCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	ZoomedFOV = 65.0f;
	ZoomInterpSpeed = 20.0f;
	WeaponAttachSocketName = "WeaponSocket";
	bDied = false;
}

// Called when the game starts or when spawned
void ASCharacter::BeginPlay()
{
	Super::BeginPlay();

	DefaultFOV = CameraComp->FieldOfView;
	HealthComp->OnHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChanged);

	if (HasAuthority())
	{
		OnDestroyed.AddDynamic(this, &ASCharacter::OnActorDestroyed);
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		for (int i = 0; i < StarterWeaponsClasses.Num(); i++)
		{
			ASWeapon* Weapon = GetWorld()->SpawnActor<ASWeapon>(StarterWeaponsClasses[i], FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
			if (Weapon)
			{
				Weapons.Add(Weapon);
				Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
				Weapon->SetOwner(this);
				if (i != 0)
					Weapon->SetActorHiddenInGame(true);
			}
		}
		
	}	
}

void ASCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ASCharacter::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void ASCharacter::BeginCrouch()
{
	Crouch();
}

void ASCharacter::EndCrouch()
{
	UnCrouch();
}

void ASCharacter::BeginADS()
{
	bWantsToZoom = true;
}

void ASCharacter::EndADS()
{
	bWantsToZoom = false;
}

void ASCharacter::ServerNextWeapon_Implementation()
{
	if (Weapons.Num() == 0)
		return;

	Weapons[CurrentWeaponIdx]->StopFire();

	uint8 OldCurrentWeaponIdx = CurrentWeaponIdx;
	CurrentWeaponIdx++;
	if (CurrentWeaponIdx == Weapons.Num())
		CurrentWeaponIdx = 0;

	Weapons[OldCurrentWeaponIdx]->SetActorHiddenInGame(true);
	Weapons[CurrentWeaponIdx]->SetActorHiddenInGame(false);
}

void ASCharacter::ServerPrevWeapon_Implementation()
{
	if (Weapons.Num() == 0)
		return;

	Weapons[CurrentWeaponIdx]->StopFire();

	uint8 OldCurrentWeaponIdx = CurrentWeaponIdx;
	CurrentWeaponIdx--;
	if (CurrentWeaponIdx == -1)
		CurrentWeaponIdx = Weapons.Num() - 1;

	Weapons[OldCurrentWeaponIdx]->SetActorHiddenInGame(true);
	Weapons[CurrentWeaponIdx]->SetActorHiddenInGame(false);

}

void ASCharacter::ServerStartFire_Implementation()
{
	if (Weapons[CurrentWeaponIdx])
		Weapons[CurrentWeaponIdx]->StartFire();
}

void ASCharacter::ServerStopFire_Implementation()
{
	if (Weapons[CurrentWeaponIdx])
		Weapons[CurrentWeaponIdx]->StopFire();
}

void ASCharacter::OnRep_Died()
{
	GetMovementComponent()->StopMovementImmediately();
	GetCharacterMovement()->GravityScale = 0.0f;
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetEnableGravity(false);
	UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (MeshComp)
		MeshComp->SetEnableGravity(false);
}

void ASCharacter::OnHealthChanged(USHealthComponent* InHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType,
	class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.0f && !bDied)
	{
		bDied = true;

		OnRep_Died();
		ServerStopFire();

		DetachFromControllerPendingDestroy();

		TArray<AActor*> OutActors;
		GetAttachedActors(OutActors);
		for (auto& Actor : OutActors)
			Actor->SetLifeSpan(10.0f);
		SetLifeSpan(10.0f);
	}
}

void ASCharacter::OnActorDestroyed(AActor* DestroyedActor)
{
	TArray<AActor*> OutActors;
	GetAttachedActors(OutActors);
	for (auto& Actor : OutActors)
		Actor->Destroy();
}

// Called every frame
void ASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float TargetFOV = bWantsToZoom ? ZoomedFOV : DefaultFOV;

	float NewFOV = FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, ZoomInterpSpeed);

	CameraComp->SetFieldOfView(NewFOV);

}

// Called to bind functionality to input
void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ASCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ASCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &ASCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &ASCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &ASCharacter::BeginCrouch);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &ASCharacter::EndCrouch);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ASCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ASCharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("ADS"), IE_Pressed, this, &ASCharacter::BeginADS);
	PlayerInputComponent->BindAction(TEXT("ADS"), IE_Released, this, &ASCharacter::EndADS);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ASCharacter::ServerStartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ASCharacter::ServerStopFire);

	PlayerInputComponent->BindAction(TEXT("NextWeapon"), IE_Pressed, this, &ASCharacter::ServerNextWeapon);
	PlayerInputComponent->BindAction(TEXT("PrevWeapon"), IE_Pressed, this, &ASCharacter::ServerPrevWeapon);
}

FVector ASCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
		return CameraComp->GetComponentLocation();

	return Super::GetPawnViewLocation();
}

void ASCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASCharacter, bDied);
	DOREPLIFETIME(ASCharacter, Weapons);
}