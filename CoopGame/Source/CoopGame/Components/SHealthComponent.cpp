// Fill out your copyright notice in the Description page of Project Settings.


#include "SHealthComponent.h"
#include "Net/UnrealNetwork.h"
#include "SGameMode.h"

// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	DefaultHealth = 100.0f;
	bIsDead = false;
	bIsImmortal = false;

	TeamNum = 255;

	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (AActor* MyOwner = GetOwner())
		if (MyOwner->HasAuthority())
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::HandleTakeAnyDamage);

	Health = DefaultHealth;	
}

void USHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || bIsImmortal || bIsDead)
		return;

	if (DamageCauser != DamagedActor && IsFriendly(DamagedActor, DamageCauser))
		return;

	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
	if (Health <= 0.0f)
		bIsDead = true;

	UE_LOG(LogTemp, Warning, TEXT("Health Changed: %s"), *FString::SanitizeFloat(Health));

	//Server / Main one 
	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);

	if (bIsDead)
		if (ASGameMode* GM = GetWorld()->GetAuthGameMode<ASGameMode>())
			GM->OnActorKilled.Broadcast(GetOwner(), DamageCauser, InstigatedBy);


	//NOTE: Not used anymore because when Health is replicated we call the function OnRep_Health, easier.
	//if (GetNetMode() == ENetMode::NM_ListenServer)
		//If we are a server, Client rpc to update the HUD
		//ClientHandleTakeAnyDamage(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);
}

void USHealthComponent::OnRep_Health(float OldHealth)
{
	float Damage = OldHealth - Health;
	OnHealthChanged.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);
}

void USHealthComponent::Heal(float HealAmount)
{
	if (HealAmount <= 0.0f || Health <= 0.0f)
		return;

	Health = FMath::Clamp(Health += HealAmount, 0.0f, DefaultHealth);
	UE_LOG(LogTemp, Warning, TEXT("Health Changed: %s (+%s)"), *FString::SanitizeFloat(Health), *FString::SanitizeFloat(HealAmount));
	OnHealthChanged.Broadcast(this, Health, -HealAmount, nullptr, nullptr, nullptr);
}

void USHealthComponent::SetIsImmortal(bool bNewIsImmortal)
{
	bIsImmortal = bNewIsImmortal;
}

void USHealthComponent::ServerSetIsImmortal_Implementation(bool bNewIsImmortal)
{
	bIsImmortal = bNewIsImmortal;
	if (AActor* MyOwner = GetOwner()) {
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Pawn %s: now is %s"), *MyOwner->GetName(), bIsImmortal ? *FString("immortal") : *FString("not immortal")));
		UE_LOG(LogTemp, Warning, TEXT("Pawn %s: now is %s"), *MyOwner->GetName(), bIsImmortal ? *FString("immortal") : *FString("not immortal"));
	}
}

float USHealthComponent::GetHealth() const
{
	return Health;
}

bool USHealthComponent::IsFriendly(AActor* ActorA, AActor* ActorB)
{
	if (!ActorA || !ActorB)
		return true;

	USHealthComponent* HealthCompA = Cast<USHealthComponent>(ActorA->GetComponentByClass(USHealthComponent::StaticClass()));
	USHealthComponent* HealthCompB = Cast<USHealthComponent>(ActorB->GetComponentByClass(USHealthComponent::StaticClass()));

	if (!HealthCompA || !HealthCompB)
		return true;

	return HealthCompA->TeamNum == HealthCompB->TeamNum;
}

void USHealthComponent::ClientHandleTakeAnyDamage_Implementation(USHealthComponent* HealthComp, float NewHealth, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	OnHealthChanged.Broadcast(HealthComp, Health, Damage, DamageType, InstigatedBy, DamageCauser);
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USHealthComponent, Health);
	DOREPLIFETIME(USHealthComponent, bIsImmortal);
}