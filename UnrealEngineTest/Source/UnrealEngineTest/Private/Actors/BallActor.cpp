// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealEngineTest/Public/Actors/BallActor.h"

// Sets default values
ABallActor::ABallActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	if (SphereMeshAsset.Succeeded())
		SphereMesh = SphereMeshAsset.Object;
	else
		UE_LOG(LogTemp, Error, TEXT("[UBallSpawner] Could not find Basic Shape (Sphere)"));

	static ConstructorHelpers::FObjectFinder<UMaterial>SphereMaterialAsset(TEXT("Material'/Engine/BasicShapes/BasicShapeMaterial.BasicShapeMaterial'"));
	if (SphereMaterialAsset.Succeeded())
		SphereMaterial = SphereMaterialAsset.Object;
	else
		UE_LOG(LogTemp, Error, TEXT("[UBallSpawner] Could not find Basic Material Shape"));

	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMesh"));
	BallMesh->SetCollisionProfileName(TEXT("Balls"));
	BallMesh->SetNotifyRigidBodyCollision(true);
	BallMesh->SetStaticMesh(SphereMesh);
	BallMesh->SetMaterial(0, SphereMaterial);
	BallMesh->OnComponentHit.AddDynamic(this, &ABallActor::OnComponentHit);
	FAttachmentTransformRules TransformRules =
		FAttachmentTransformRules(EAttachmentRule::KeepRelative, false);
	SetRootComponent(BallMesh);

	Tags.Add(FName("Ball"));
}

void ABallActor::Configure(	FVector NewSphereScale, float NewTimeToApplyPhysics, bool bNewbSphereDiesAfterPhysiscs, float NewSphereLifeSpanAfterPhysics)
{
	SphereScale = NewSphereScale;
	TimeToApplyPhysics = NewTimeToApplyPhysics;
	bSphereDiesAfterPhysiscs = bNewbSphereDiesAfterPhysiscs;
	SphereLifeSpanAfterPhysics = NewSphereLifeSpanAfterPhysics;

	BallMesh->SetWorldScale3D(SphereScale);

	FTimerHandle THandle;
	GetWorld()->GetTimerManager().SetTimer(THandle, this, &ABallActor::ApplyPhysics, TimeToApplyPhysics);
}

void ABallActor::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (!OtherActor->Tags.Contains(FName("Ball")))
		return;

	// Two balls collided, calculate the next spawnLocation, and volume.
	FVector SpawnLocation = (HitComponent->GetComponentLocation() + OtherComp->GetComponentLocation()) / 2.0f;
	// Get the radius of the balls (A sphere with a scale of 1, measures 1 unit in unreal so the scale is the diameter)
	float Radius1 = HitComponent->K2_GetComponentScale().X / 2.0f;
	float Radius2 = OtherComp->K2_GetComponentScale().X / 2.0f;
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Two balls collided Radius 1: (%f) Radius 2: (%f)!"), Radius1, Radius2));
	float Volume1 = 4.0 / 3.0 * PI * FMath::Pow(Radius1, 3);
	float Volume2 = 4.0 / 3.0 * PI * FMath::Pow(Radius2, 3);
	float NewBallVolume = Volume1 + Volume2;
	float NewBallRadius = FMath::Pow(NewBallVolume * 3.0f / 4.0f / PI, 1.0f / 3.0f);

	// Delete the two old balls.
	UStaticMeshComponent* OtherBallMesh = Cast<UStaticMeshComponent>(HitComponent->GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	OtherBallMesh->SetSimulatePhysics(false);
	HitComponent->GetOwner()->Destroy();

	OtherBallMesh = Cast<UStaticMeshComponent>(OtherActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	OtherBallMesh->SetSimulatePhysics(false);
	OtherActor->Destroy();

	// Create a new one and configure it.
	ABallActor* NewBall = GetWorld()->SpawnActor<ABallActor>(SpawnLocation, FRotator(), FActorSpawnParameters());
	NewBall->Configure(SphereScale, 0.001f, bSphereDiesAfterPhysiscs, SphereLifeSpanAfterPhysics);
	OtherBallMesh = Cast<UStaticMeshComponent>(NewBall->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	OtherBallMesh->SetWorldScale3D(FVector(NewBallRadius * 2.0f));
	if (SphereMaterial)
	{
		UMaterialInstanceDynamic* dynamicMaterial = UMaterialInstanceDynamic::Create(SphereMaterial, this);
		OtherBallMesh->SetMaterial(0, dynamicMaterial);
		dynamicMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Green);
	}
}

void ABallActor::ApplyPhysics()
{
	BallMesh->SetSimulatePhysics(true);

	if (bSphereDiesAfterPhysiscs)
		SetLifeSpan(SphereLifeSpanAfterPhysics);
}

