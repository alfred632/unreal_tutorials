// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealEngineTest/Public/Components/BallSpawner.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "UnrealEngineTest/Public/Actors/BallActor.h"

// Sets default values for this component's properties
UBallSpawner::UBallSpawner()
{
	PrimaryComponentTick.bCanEverTick = true;

	BallsToSpawnCount = 20;
	SphereSpawnRadius = 2.0f;
	SphereScale = FVector(0.2f);
	TimeToApplyPhysics = 2.0f;
	bSphereDiesAfterPhysiscs = true;
	SphereLifeSpanAfterPhysics = 5.0f;
	BallSpawnerSocketName = "BallSpawnerSocket";
}

void UBallSpawner::SpawnBalls()
{
	for (int i = 0; i < BallsToSpawnCount; i++)
		SpawnBall();
}

ABallActor* UBallSpawner::SpawnBall()
{
	//Pick random location inside sphere
	FVector SpawnPosition = GenerateSpawnPosition();
	 
	//Create Actor and configure it
	ABallActor* NewBall = GetWorld()->SpawnActor<ABallActor>(SpawnPosition, FRotator(), FActorSpawnParameters());
	NewBall->Configure(SphereScale, TimeToApplyPhysics, bSphereDiesAfterPhysiscs, SphereLifeSpanAfterPhysics);	
	
	return NewBall;
}

FVector UBallSpawner::GenerateSpawnPosition()
{
	//Get socket position or this component location
	FVector BasePosition;
	if (bUseSocketName)
	{
		USkeletalMeshComponent* SkelMesh = Cast<USkeletalMeshComponent>(GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		BasePosition = SkelMesh->GetSocketLocation(BallSpawnerSocketName);
	}
	else
		BasePosition = GetComponentLocation();

	//Generate random sphere point, using the following the last method from this post
	//https://karthikkaranth.me/blog/generating-random-points-in-a-sphere/
	float u = FMath::FRand();
	float v = FMath::FRand();
	float theta = u * 2.0 * PI;
	float phi = FMath::Acos(2.0 * v - 1.0);
	float r = FMath::Pow(FMath::FRand(), 1.0f / 3.0f);
	float sinTheta = FMath::Sin(theta);
	float cosTheta = FMath::Cos(theta);
	float sinPhi = FMath::Sin(phi);
	float cosPhi = FMath::Cos(phi);
	float x = r * sinPhi * cosTheta * SphereSpawnRadius;
	float y = r * sinPhi * sinTheta * SphereSpawnRadius;
	float z = r * cosPhi * SphereSpawnRadius;

	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Random Sphere position (%f, %f, %f)"), x, y, z));
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Random Sphere position (%f, %f, %f)"), x + BasePosition.X, y + BasePosition.Y, z + BasePosition.Z));
	return FVector(x + BasePosition.X, y + BasePosition.Y, z + BasePosition.Z);
}

