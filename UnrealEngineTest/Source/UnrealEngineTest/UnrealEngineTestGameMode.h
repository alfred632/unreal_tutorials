// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealEngineTestGameMode.generated.h"

UCLASS(minimalapi)
class AUnrealEngineTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnrealEngineTestGameMode();
};



