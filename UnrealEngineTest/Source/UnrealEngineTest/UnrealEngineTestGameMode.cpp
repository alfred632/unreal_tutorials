// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealEngineTestGameMode.h"
#include "UnrealEngineTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUnrealEngineTestGameMode::AUnrealEngineTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
