// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "BallSpawner.generated.h"

class ABallActor;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALENGINETEST_API UBallSpawner : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBallSpawner();

	void SpawnBalls();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	int32 BallsToSpawnCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	float SphereSpawnRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	FVector SphereScale;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	float TimeToApplyPhysics;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	bool bSphereDiesAfterPhysiscs;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner", meta = (EditCondition = "bSphereDiesAfterPhysiscs"))
	float SphereLifeSpanAfterPhysics;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner", meta = (EditCondition = "bUseSocketName"))
	FName BallSpawnerSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	bool bUseSocketName;

	ABallActor* SpawnBall();

	FVector GenerateSpawnPosition();	
};
