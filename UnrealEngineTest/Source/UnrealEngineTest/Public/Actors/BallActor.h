// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BallActor.generated.h"

UCLASS()
class UNREALENGINETEST_API ABallActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABallActor();

	void Configure(FVector NewSphereScale, float NewTimeToApplyPhysics, bool bNewbSphereDiesAfterPhysiscs, float NewSphereLifeSpanAfterPhysics);

	UFUNCTION()
	void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
		const FHitResult& Hit);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallMesh")
	UStaticMeshComponent* BallMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	FVector SphereScale;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	float TimeToApplyPhysics;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner")
	bool bSphereDiesAfterPhysiscs;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BallSpawner", meta = (EditCondition = "bSphereDiesAfterPhysiscs"))
	float SphereLifeSpanAfterPhysics;

	void ApplyPhysics();

private:
	UStaticMesh* SphereMesh;
	UMaterial* SphereMaterial;
};
