// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSLaunchPad.h"
#include "Components/BoxComponent.h"
#include "FPSCharacter.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFPSLaunchPad::AFPSLaunchPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LaunchPadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LaunchPadMesh"));
	LaunchPadMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = LaunchPadMesh;

	LaunchPadTopMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LaunchPadTopMesh"));
	LaunchPadTopMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LaunchPadTopMesh->SetupAttachment(LaunchPadMesh);

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(LaunchPadMesh);
	CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AFPSLaunchPad::OnComponentBeginOverlap);

	LaunchDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("LaunchDirection"));
	LaunchDirection->SetupAttachment(LaunchPadMesh);

}

// Called when the game starts or when spawned
void AFPSLaunchPad::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFPSLaunchPad::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
	if (MyCharacter)
	{
		MyCharacter->LaunchCharacter(LaunchDirection->GetForwardVector() * LaunchVelocity, false, false);
	}
	//If not character check if physics enabled
	else if (OtherComp->IsSimulatingPhysics())
	{
		//OtherComp->AddForce(LaunchDirection->GetForwardVector() * LaunchVelocity);
		//OtherComp->AddImpulseAtLocation(LaunchDirection->GetForwardVector() * LaunchVelocity, GetActorLocation());
		OtherComp->AddImpulse(LaunchDirection->GetForwardVector() * LaunchVelocity, NAME_None, true);
	}

	UGameplayStatics::SpawnEmitterAtLocation(this, LaunchEmitter, GetActorLocation());
}

// Called every frame
void AFPSLaunchPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

