// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSGameState.h"
#include "Engine/World.h"
#include "FPSPlayerController.h"

void AFPSGameState::MulticastOnMissionComplete_Implementation(APawn* InstigatorPawn, bool bMissionSuccess)
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		if (AFPSPlayerController* PC = Cast<AFPSPlayerController>(It->Get()))
		{
			PC->OnMissionCompleted(InstigatorPawn, bMissionSuccess);
			if (APawn* Pawn = PC->GetPawn())
				Pawn->DisableInput(nullptr);
		}
	}
}
