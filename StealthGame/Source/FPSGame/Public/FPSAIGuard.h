// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSAIGuard.generated.h"

class UPawnSensingComponent;

UENUM(BlueprintType)
enum class EAIState : uint8
{
	Idle,
	Suspicious,
	Alerted
};

UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

protected:

	FRotator OriginalRotation;
	FTimerHandle TimerHandle_ResetOrientation;
	UPROPERTY(ReplicatedUsing=OnRep_GuardState)
	EAIState GuardState;

	UPROPERTY(EditInstanceOnly, Category = "Patrol", meta = (EditCondition="bIsPatrol"))
	TArray<AActor*> PatrolPoints;
	int32 CurrentPatrolPointIdx;
	UPROPERTY(EditInstanceOnly, Category = "Patrol")
	bool bIsPatrol;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPawnSensingComponent* PawnSensingComp;

	UFUNCTION()
	void OnPawnSeen(APawn* SeenPawn);

	UFUNCTION()
	void OnNoiseHear(APawn* NoiseInstigator, const FVector& Location, float Volume);

	void ResetOrientation();

	void SetGuardState(EAIState NewState);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnStateChanged(EAIState NewState);

	void MoveToNextPatrolPoint();

	UFUNCTION()
	void OnRep_GuardState();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
