// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class AGun;
enum EWeaponTypes;

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	UFUNCTION(BlueprintPure)
	int GetCurrentAmmo() const;

	UFUNCTION(BlueprintPure)
	int GetCurrentMagAmmo() const;

	UFUNCTION(BlueprintPure)
	float GetReloadPercentage() const;

	void PullTrigger();
	UFUNCTION(BlueprintCallable)
	void ReleaseTrigger();

	void AddAmmo(EWeaponTypes WeaponType, int Quantity);

private:

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);

	void DisableActor(AActor* Actor);
	void EnableActor(AActor* Actor);

	void NextWeapon();
	void PrevWeapon();
	void ReloadWeapon();

	void SpawnShooterWeapons();

	UPROPERTY(EditAnywhere)
	float RotationRate = 10.0f;
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100.0f;
	UPROPERTY(VisibleAnywhere)
	float Health;

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<AGun>> GunsClasses;

	UPROPERTY(VisibleAnywhere)
	TArray<AGun*> Guns;

	UPROPERTY(VisibleAnywhere)
	int CrtWpnIdx;
};
