// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"

void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	HudScreen->RemoveFromViewport();
	if (bIsWinner)
	{
		UUserWidget* WinScreen = CreateWidget(this, WinScreenClass);
		if (WinScreen)
		{
			WinScreen->AddToViewport();
		}
	}
	else 
	{
		UUserWidget* LoseScreen = CreateWidget(this, LoseScreenClass);
		if (LoseScreen)
		{
			LoseScreen->AddToViewport();
		}
	}
	
	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}

UUserWidget* AShooterPlayerController::GetHudScreen()
{
	return HudScreen;
}

void AShooterPlayerController::Pause()
{
	if (!PauseMenuScreen)
		PauseMenuScreen = CreateWidget(this, PauseMenuClass);

	if (!PauseMenuScreen->IsVisible())
		PauseMenuScreen->AddToViewport();
	else
	{
		PauseMenuScreen->RemoveFromParent();
		HudScreen->SetVisibility(ESlateVisibility::Visible);
	}
}

void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	HudScreen = CreateWidget(this, HudClass);
	if (HudScreen)
		HudScreen->AddToViewport();
}

void AShooterPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	bBlockInput = false;

	InputComponent->BindAction(TEXT("Pause"), IE_Pressed, this, &AShooterPlayerController::Pause);
}
