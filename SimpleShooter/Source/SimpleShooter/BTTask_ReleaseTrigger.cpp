// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_ReleaseTrigger.h"
#include "AIController.h"
#include "ShooterCharacter.h"

UBTTask_ReleaseTrigger::UBTTask_ReleaseTrigger()
{
	NodeName = TEXT("Release Trigger");
}

EBTNodeResult::Type UBTTask_ReleaseTrigger::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	if (!OwnerComp.GetAIOwner()) return EBTNodeResult::Failed;

	AShooterCharacter* Character = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (!Character) return EBTNodeResult::Failed;

	Character->ReleaseTrigger();
	return EBTNodeResult::Succeeded;
}

EBTNodeResult::Type UBTTask_ReleaseTrigger::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	if (!OwnerComp.GetAIOwner()) return EBTNodeResult::Aborted;

	AShooterCharacter* Character = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (!Character) return EBTNodeResult::Aborted;

	Character->ReleaseTrigger();
	return EBTNodeResult::Aborted;
}
