// Fill out your copyright notice in the Description page of Project Settings.


#include "GunProjectile.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AGunProjectile::AGunProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gun Projectile Mesh"));
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AGunProjectile::OnHit);
	RootComponent = ProjectileMesh;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->InitialSpeed = MovementSpeed;
	ProjectileMovement->MaxSpeed = MovementSpeed;

	InitialLifeSpan = 30.0f;
}

// Called when the game starts or when spawned
void AGunProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGunProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	AActor* MyOwner = GetOwner();
	if (!MyOwner)
		return;

	//Damage
	if (OtherActor && OtherActor != this && OtherActor != MyOwner)
	{
		//Sweep at location for other actors
		TArray<FHitResult> TraceHits;
		UKismetSystemLibrary::SphereTraceMultiForObjects(GetWorld(), GetActorLocation(), GetActorLocation(), OuterRadius, ObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::None, TraceHits, true);

		if (TraceHits.Num() > 0) {
			AController* OwnerController;
			APawn* OwnerPawn = Cast<APawn>(GetOwner());
			if (OwnerPawn == nullptr)
				OwnerController = nullptr;
			else
				OwnerController = OwnerPawn->GetController();

			FRadialDamageEvent DamageEvent;
			FRadialDamageParams DamageParams(Damage, MinDamage, InnerRadius, OuterRadius, DamageFalloff);
			DamageEvent.Params = DamageParams;

			//Send radial damage event to other actors
			TArray<AActor*> UniqueActors;
			for (auto& TraceHit : TraceHits) {
				if (UniqueActors.Contains(TraceHit.Actor.Get()))
					continue;
	
				UniqueActors.Emplace(TraceHit.Actor.Get());
				TraceHit.Actor->TakeDamage(Damage, DamageEvent, OwnerController, this);
				UE_LOG(LogTemp, Warning, TEXT("Actor name: %s"), *TraceHit.Actor->GetName());
			}
		}
		
	}

	//Explode
	UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());
	GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(HitShake);
	Destroy();
}

// Called every frame
void AGunProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

