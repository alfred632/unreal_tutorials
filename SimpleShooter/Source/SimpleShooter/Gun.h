// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

class  AGunProjectile;

UENUM()
enum EWeaponTypes
{
	AssaultRifle,
	Sniper,
	GrenadeLauncher
};

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

	void PullTrigger();
	void ReleaseTrigger();

	int GetCurrentAmmo() const;
	int GetCurrentMagAmmo() const;
	float GetReloadPercentage() const;

	void HolsterWeapon();
	void Reload();

	void DeattachFromOwner();

	void AddAmmo(int Quantity);

	EWeaponTypes GetWeaponType();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(VisibleAnywhere)
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, Category = "Visuals And Effects")
	USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, Category = "Visuals And Effects");
	UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere, Category = "Visuals And Effects");
	USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere, Category = "Visuals And Effects", meta = (EditCondition = "!bUsesProjectile"))
	UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere, Category = "Visuals And Effects", meta = (EditCondition = "!bUsesProjectile"));
	USoundBase* ImpactSound;

	UPROPERTY(EditDefaultsOnly, Category = "Config")
	bool bUsesProjectile = false;
	UPROPERTY(EditAnywhere, Category = "Config", meta = (EditCondition = "!bUsesProjectile"))
	float MaxRange = 1000.f;
	UPROPERTY(EditAnywhere, Category = "Config", meta = (EditCondition = "bUsesProjectile"))
	float Damage = 10.0f;
	UPROPERTY(EditAnywhere, Category = "Config", meta = (EditCondition = "bUsesProjectile"))
	TSubclassOf<AGunProjectile> GunProjectile;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USceneComponent* ProjectileSpawnPoint;


	UPROPERTY(EditAnywhere, Category = "Config")
	int32 MaxAmmo = 200;
	UPROPERTY(EditAnywhere, Category = "Config")
	int32 CurrentAmmo;
	UPROPERTY(EditAnywhere, Category = "Config")
	int32 MaxMagAmmo = 30;
	UPROPERTY(EditAnywhere, Category = "Config")
	int32 CurrentMagAmmo;
	
	UPROPERTY(EditAnywhere, Category = "Config")
	float RateOfFire = 1.5f;
	UPROPERTY()
	float NextTimeToShoot;
	UPROPERTY()
	float ShootDelay;
	UPROPERTY(EditAnywhere, Category = "Config")
	bool bIsAutomatic = false;
	bool bIsShooting = false;
	bool bShotAndRelease = true;

	UPROPERTY(EditAnywhere, Category = "Config")
	float ReloadDelay = 1.0f;
	float TimeFinishReload = 0.0f;
	float TimeReloadStarted = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Config")
	TEnumAsByte<EWeaponTypes> WeaponType;
	UPROPERTY(EditAnywhere, Category = "Config")
	int32 PickUpAmmoQuantity = 10;

	UPROPERTY(VisibleAnywhere, Category = "Config")
	bool bHasBeenDropped = false;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);
	bool GunTrace(FHitResult& Hit, FVector& ShotDirection, float Range);
	void CheckAndFire();

	void ShootTrace();
	void ShootProjectile();

	bool CheckFireConditions();

	bool CheckReload();
	void StartReload();
	void FinishReload();

	void InterruptReload();

	UFUNCTION()
	void OnPickUp(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	AController* GetOwnerController() const;

};
