// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooterGameModeBase.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	CrtWpnIdx = 0;

	//Hide default gun mesh on actor
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_MAX);
	
	SpawnShooterWeapons();

	Health = MaxHealth;
	}

//Spawn all the weapons and disable them. Then enable the current one
void AShooterCharacter::SpawnShooterWeapons()
{
	int idx = 0;
	for(auto& GunClass : GunsClasses) {
		Guns.Insert(GetWorld()->SpawnActor<AGun>(GunClass), idx);
		Guns[idx]->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
		Guns[idx]->SetOwner(this);

		DisableActor(Guns[idx]);
		idx++;
	}
	
	if (Guns[CrtWpnIdx])
		EnableActor(Guns[CrtWpnIdx]);
}

void AShooterCharacter::DisableActor(AActor* Actor)
{
	Actor->SetActorHiddenInGame(true);
	Actor->SetActorEnableCollision(false);
	Actor->SetActorTickEnabled(false);
}

void AShooterCharacter::EnableActor(AActor* Actor)
{
	Actor->SetActorHiddenInGame(false);
	Actor->SetActorEnableCollision(true);
	Actor->SetActorTickEnabled(true);
}

void AShooterCharacter::NextWeapon()
{
	Guns[CrtWpnIdx]->HolsterWeapon();
	DisableActor(Guns[CrtWpnIdx]);
	if (++CrtWpnIdx == Guns.Num())
		CrtWpnIdx = 0;
	EnableActor(Guns[CrtWpnIdx]);
}

void AShooterCharacter::PrevWeapon()
{
	Guns[CrtWpnIdx]->HolsterWeapon();
	DisableActor(Guns[CrtWpnIdx]);
	if (--CrtWpnIdx < 0)
		CrtWpnIdx = Guns.Num() - 1;
	EnableActor(Guns[CrtWpnIdx]);
}

void AShooterCharacter::ReloadWeapon()
{
	if (Guns[CrtWpnIdx])
		Guns[CrtWpnIdx]->Reload();
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Pressed, this, &AShooterCharacter::PullTrigger);
	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Released, this, &AShooterCharacter::ReleaseTrigger);
	PlayerInputComponent->BindAction(TEXT("NextWeapon"), IE_Pressed, this, &AShooterCharacter::NextWeapon);
	PlayerInputComponent->BindAction(TEXT("PrevWeapon"), IE_Pressed, this, &AShooterCharacter::PrevWeapon);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AShooterCharacter::ReloadWeapon);
}

float AShooterCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageApplied = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	
	Health = Health - FMath::Min(Health, DamageApplied);

	if (IsDead())
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
		if (GameMode)
			GameMode->PawnKilled(this);
		
		if (Guns[CrtWpnIdx]) {
			Guns[CrtWpnIdx]->ReleaseTrigger();
			Guns[CrtWpnIdx]->DeattachFromOwner();
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageApplied;
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0.0f;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

int AShooterCharacter::GetCurrentAmmo() const
{
	if (Guns[CrtWpnIdx])
		return Guns[CrtWpnIdx]->GetCurrentAmmo();
	return 0;
}

int AShooterCharacter::GetCurrentMagAmmo() const
{
	if (Guns[CrtWpnIdx])
		return Guns[CrtWpnIdx]->GetCurrentMagAmmo();
	return 0;
}

float AShooterCharacter::GetReloadPercentage() const
{
	if (Guns[CrtWpnIdx])
		return Guns[CrtWpnIdx]->GetReloadPercentage();
	return 0.0f;
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::PullTrigger()
{
	Guns[CrtWpnIdx]->PullTrigger();
}

void AShooterCharacter::ReleaseTrigger()
{
	Guns[CrtWpnIdx]->ReleaseTrigger();
}

void AShooterCharacter::AddAmmo(EWeaponTypes WeaponType, int Quantity)
{
	for (auto& Gun : Guns)
	{
		if (Gun->GetWeaponType() == WeaponType)
		{
			Gun->AddAmmo(Quantity);
			return;
		}
	}
}
