// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "GunProjectile.h"
#include "ShooterCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun Mesh"));
	Mesh->SetupAttachment(Root);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(Mesh);
}

void AGun::PullTrigger()
{
	bIsShooting = true;
}

void AGun::ReleaseTrigger()
{
	bIsShooting = false;

	if (!bIsAutomatic)
		bShotAndRelease = true;
}

int AGun::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int AGun::GetCurrentMagAmmo() const
{
	return CurrentMagAmmo;
}

float AGun::GetReloadPercentage() const
{
	if (TimeFinishReload == 0.0f)
		return 0.0f;
	else
		return (ReloadDelay - (TimeFinishReload - UGameplayStatics::GetRealTimeSeconds(GetWorld()))) / ReloadDelay;
}

void AGun::HolsterWeapon()
{
	InterruptReload();
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmo = MaxAmmo;
	CurrentMagAmmo = MaxMagAmmo;
	ShootDelay = 1.0f / RateOfFire;
	NextTimeToShoot = UGameplayStatics::GetRealTimeSeconds(GetWorld());

	Mesh->OnComponentBeginOverlap.AddUniqueDynamic(this, &AGun::OnPickUp);
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CheckReload())
		return;

	if (bIsShooting)
		CheckAndFire();
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	return GunTrace(Hit, ShotDirection, MaxRange);
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection, float Range)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr) return false;

	FVector ViewLocation;
	FRotator ViewRotation;
	OwnerController->GetPlayerViewPoint(ViewLocation, ViewRotation);
	ShotDirection = -ViewRotation.Vector();

	FVector End = ViewLocation + ViewRotation.Vector() * MaxRange;

	Hit;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(Hit, ViewLocation, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

void AGun::CheckAndFire()
{
	if (!CheckFireConditions())
		return;

	CurrentMagAmmo--;

	//Maybe this causes a bug. Less fps will equal to less shoots I think but this isn't a multiplayer so...
	float CurrentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	NextTimeToShoot = CurrentTime + ShootDelay;

	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	if (bUsesProjectile)
		ShootProjectile();
	else
		ShootTrace();

}

void AGun::ShootTrace()
{
	FHitResult Hit;
	FVector ShotDirection;
	if (GunTrace(Hit, ShotDirection))
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Hit.Location);

		if (AActor* HitActor = Hit.GetActor()) {
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
			AController* OwnerController = GetOwnerController();
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
		}
	}
}

void AGun::ShootProjectile()
{
	if (!GunProjectile)
		return;

	//Default location and rotation
	FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
	FRotator SpawnRotation = ProjectileSpawnPoint->GetComponentRotation();

	//Check if we hit something from the camera so we can modify the projectile rotation
	FHitResult Hit;
	FVector ShotDirection;
	if (GunTrace(Hit, ShotDirection, 100000.0f)) {
		FVector NewDirection = (Hit.Location - SpawnLocation).GetSafeNormal();
		SpawnRotation = NewDirection.Rotation();
	}
	
	AGunProjectile* TempProjectile = GetWorld()->SpawnActor<AGunProjectile>(GunProjectile, SpawnLocation, SpawnRotation);
	TempProjectile->SetOwner(this);
}

bool AGun::CheckFireConditions()
{
	//Check RateOfFire Time
	float CurrentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	if (CurrentTime < NextTimeToShoot)
		return false;

	//Check Automatic. If it is not, check if it still needs to release the trigger
	if (!bIsAutomatic) {
		if (!bShotAndRelease)
			return false;
		else
			bShotAndRelease = false;
	}

	return true;
}

bool AGun::CheckReload()
{
	//We are reloading
	if (TimeFinishReload != 0.0f) {
		if (TimeFinishReload < UGameplayStatics::GetRealTimeSeconds(GetWorld()))
			FinishReload();
		return true;
	}

	//Check Ammo  for auto reload
	if (CurrentMagAmmo == 0) {
		// Start reloading if it's not already
		if (TimeFinishReload == 0.0f) 
			StartReload();
		return true;
	}

	return false;
}

void AGun::Reload()
{
	if (CurrentMagAmmo != MaxMagAmmo && TimeFinishReload == 0)
		StartReload();
}

void AGun::DeattachFromOwner()
{
	SetOwner(nullptr);
	FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld, true);
	DetachFromActor(DetachmentTransformRules);
	Mesh->SetSimulatePhysics(true);
	Mesh->SetGenerateOverlapEvents(true);
	bHasBeenDropped = true;
}

void AGun::AddAmmo(int Quantity)
{
	CurrentAmmo += Quantity;
}

EWeaponTypes AGun::GetWeaponType()
{
	return WeaponType;
}

void AGun::StartReload()
{
	TimeReloadStarted = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	TimeFinishReload = TimeReloadStarted + ReloadDelay;
}

void AGun::FinishReload()
{
	TimeFinishReload = 0.0f;
	TimeReloadStarted = 0.0f;
	int32 PrevCurrentMagAmmo = CurrentMagAmmo;
	CurrentMagAmmo = FMath::Min(CurrentAmmo + CurrentMagAmmo, MaxMagAmmo);
	CurrentAmmo = CurrentAmmo - CurrentMagAmmo + PrevCurrentMagAmmo;
}

void AGun::InterruptReload()
{
	TimeFinishReload = 0.0f;
	TimeReloadStarted = 0.0f;
}

void AGun::OnPickUp(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bHasBeenDropped)
		return;

	if (OtherActor == GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
		ShooterCharacter->AddAmmo(WeaponType, PickUpAmmoQuantity);
		Destroy();
	}
}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr) return nullptr;

	return OwnerPawn->GetController();
}

